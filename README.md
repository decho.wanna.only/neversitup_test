# Neversitup Test

### `yarn install`

install package

### `yarn test`

run project

# Detail work

## Test Permutations

File src/service/permutations

Show coding logic and unit test Permutations (Choose your the most skilled)
your task is to create all permutations of a non-empty input string and remove duplicates, if present.
Create as many "shufflings" as you can!

## Test Find the odd int

File src/service/findOdd

Show coding logic and unit test Find the odd int (Choose your the most skilled)
Given an array of integers, find the one that appears an odd number of times.

There will always be only one integer that appears an odd number of times.

## Test Count the smiley faces!

File src/service/countSmileys

Show coding logic and unit test Count the smiley faces! (Choose your the most skilled)
Given an array (arr) as an argument complete the function countSmileys that should return the total number of smiling faces.

Rules for a smiling face:
Each smiley face must contain a valid pair of eyes. Eyes can be marked as : or ;
A smiley face can have a nose but it does not have to. Valid characters for a nose are - or ~
Every smiling face must have a smiling mouth that should be marked with either ) or D

No additional characters are allowed except for those mentioned.

Valid smiley face examples: :) :D ;-D :~)
Invalid smiley faces: ;( :> :} :]

## Main Test

File src/service/main.test

Config value Test and call function test
