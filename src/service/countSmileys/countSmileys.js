function countSmileys(arr) {
  var smileys = [
    ':)',
    ';)',
    ':-)',
    ';-)',
    ';~)',
    ':~)',
    ':D',
    ';D',
    ':~D',
    ';-D',
    ':-D',
    ';~D',
  ];
  var empty = 0;
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < smileys.length; j++) {
      if (arr[i] === smileys[j]) {
        empty++;
      }
    }
  }
  return empty;
}

module.exports = {
  countSmileys,
};
