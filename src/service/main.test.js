const permutations = require('./permutations/stringPermutations');
const findOddInt = require('./findOdd/findOddInt');
const countSmileys = require('./countSmileys/countSmileys');
describe('Unit Test', () => {
  let inputStr = 'aabb';
  let arrayOddInt = [1, 2, 2, 3, 3, 3, 4, 3, 3, 3, 2, 2, 1, 4, 4, 4, 4];
  let arrayCountSmileys = [';]', ':[', ';*', ':$', ';-D'];
  test(`With input ${inputStr} :`, async () => {
    var str = permutations.stringPermutations(inputStr);
    await new Promise((resolve) => {
      resolve(console.log('Your function should return:', str));
    });
  });
  test(`Find the odd int`, async () => {
    var oddInt = findOddInt.findOddInt(arrayOddInt);
    await new Promise((resolve) => {
      resolve(
        console.log(`[${arrayOddInt}] should return ${oddInt.arr} because it occurs ${oddInt.count} time (which is odd).
      `)
      );
    });
  });
  test(`Count the smiley faces`, async () => {
    var _countSmileys = countSmileys.countSmileys(arrayCountSmileys);
    await new Promise((resolve) => {
      resolve(
        console.log(`should return . ${_countSmileys}
      `)
      );
    });
  });
});
