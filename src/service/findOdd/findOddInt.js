const findOddInt = (int) => {
  for (let i = 0; i < int.length; i++) {
    let count = 0;
    for (let j = 0; j < int.length; j++) {
      if (int[i] === int[j]) {
        count++;
      }
    }
    if (count % 2 !== 0) {
      return {
        count: count,
        arr: int[i],
      };
    }
  }
};

module.exports = {
  findOddInt,
};
